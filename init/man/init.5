.TH init 5 2010-02-04 "Upstart"
.\"
.SH NAME
init \- Upstart init daemon job configuration
.\"
.SH SYNOPSIS
.B /etc/init
.\"
.SH DESCRIPTION
On startup, the Upstart
.BR init (8)
daemon reads its job configuration from the
.I /etc/init
directory, and watches for future changes using
.BR inotify (7).

Files in this directory must end in
.I .conf
and may also be present in sub-directories.

Each file defines a single service or task, with the name taken from its
relative path within the directory without the extension.  For example a
job defined in
.I /etc/init/rc-sysinit.conf
is named
.IR rc-sysinit ,
while a job defined in
.I /etc/init/net/apache.conf
is named
.IR net/apache .

These files are plain text and should not be executable.
.\"
.SS Format
Each line begins with a configuration stanza and continues until either
the end of the line or a line containing a closing stanza.  Line breaks
within a stanza are permitted within single or double quotes, or if
preceeded by a blackslash.

Stanzas and their arguments are delimited by whitespace, which consists
of one or more space or tab characters which are otherwise ignored unless
placed within single or double quotes.

Comments begin with a `#' and continue until the end of the line.  Blank
lines and lines consisting only of whitespace or comments are ignored.
.\"
.SS Process definition
The primary use of jobs is to define services or tasks to be run by the
.BR init (8)
daemon.  Each job may have one or more different processes run as part
of its lifecycle, with the most common known as the main process.

The main process is defined using either the
.B exec
or
.B script
stanzas, only one of which is permitted.  These specify the executable
or shell script that will be run when the job is considered to be running.
Once this process terminates, the job stop.

All processes are run with the full job environment available as
environment variables in their process.

.TP
.B exec \fICOMMAND \fR[ \fIARG \fR]...
This stanza defines the process to be run as the name of an executable
on the filesystem, and zero or more arguments to be passed to it.  Any
special characters, e.g. quotes or `$' specified will result in the
entire command being passed to a shell for expansion.

.nf
exec /usr/sbin/acpid -c $EVENTSDIR -s $SOCKET
.fi
.\"
.TP
.B script \fR... \fBend script
This stanza defines the process to be run as a shell script that will
be executed using
.BR sh(1).
The
.I -e
shell option is always used, so any command that fails will terminate
the script.

The
.B script
stanza appears on its own on a line, the script is everything up until
the first
.B end script
stanza appearing on its own on a line.

.nf
script
    dd bs=1 if=/proc/kmsg of=$KMSGSINK
    exec /sbin/klogd -P $KMSGSINK
end script
.fi

.PP
There an additional four processes that may be run as part of the job's
lifecycle.  These are specified as the process name, followed by an
.B exec
or
.B script
stanza.

.TP
.B pre-start exec\fR|\fBscript\fR...
This process will be run after the job's
.BR starting (7)
event has finished, but before the main process is run.  It is typically
used to prepare the environment, such as making necessary directories.
.\"
.TP
.B post-start exec\fR|\fBscript\fR...
This process will be run before the job's
.BR started (7)
event is emitted, but after the main process has been spawned.  It is
typically used to send necessary commands to the main process, or to
delay the
.BR started (7)
event until the main process is ready to receive clients.
.\"
.TP
.B pre-stop exec\fR|\fBscript\fR...
This process is run if the job is stopped by an event listed in its
.B stop on
stanza or by the
.BR stop (8)
command.  It will be run before the job's
.BR stopping (7)
event is emitted and before the main process is killed.  It is typically
used to send any necessary shutdown commands to the main process, and it
may also call the
.BR start (8)
command without arguments to cancel the stop.
.\"
.TP
.B post-stop exec\fR|\fBscript\fR...
This process is run after the main process has been killed and before
the job's
.BR stopped (7)
event is emitted.  It is typically used to clean up the environment,
such as removing temporary directories.

.PP
All of these process, including the main process, are optional.  Services
without a main process will appear to be running until they are stopped,
this is commonly used to define states such as runlevels.  It's quite
permissable to have no main process, but to have
.B pre-start
and
.B post-stop
processes for the state.

.RS
.nf
pre-start exec ifup -a
post-stop exec ifdown -a
.fi
.RE
.\"
.SS Event definition
Jobs can be manually started and stopped at any time by a system adminstrator
using the.
.BR start (8)
and
.BR stop (8)
tools, however it is far more useful for jobs to be started and stopped
automatically by the
.BR init (8)
daemon when necessary.

This is done by specifying which events should cause your job to be
started, and which cause your process to be stopped again.

The set of possible events is limitless, however there are a number of
standard events defined by the
.BR init (8)
daemon and
.BR telinit (8)
tools that you will want to use.

When first started, the
.BR init (8)
daemon will emit the
.BR startup (7)
event.  This will activate jobs that implement System V compatibility and
the
.BR runlevel (7)
event.  As jobs are started and stopped, the
.BR init (8)
daemon will emit the
.BR starting (7),
.BR started (7),
.BR stopping (7)
and
.BR stopped (7)
events on their behalf.

.TP
.B start on \fIEVENT \fR[[\fIKEY=\fR]\fIVALUE\fR]... [\fBand\fR|\fBor\fR...]
The
.B start on
stanza defines the set of events that will cause the job to be automatically
started.  Each
.I EVENT
is given by its name.  Multiple events are permitted using the
.B and
&
.B or
operators, and complex expressions may be performed with parentheses (within
which line breaks are permitted).

You may also match on the environment variables contained within the event
by specifying the
.I KEY
and expected
.IR VALUE .
If you know the order in which the variables are given to the event you may
omit the
.IR KEY .

.I VALUE
may contain wildcard matches and globs as permitted by
.BR fnmatch (3)
and may expand the value of any variable defined with the
.B env
stanza.

Negation is permitted by using
.I !=
between the
.I KEY
and
.IR VALUE .

.nf
start on started gdm or started kdm

start on device-added SUBSYSTEM=tty DEVPATH=ttyS*

start on net-device-added INTERFACE!=lo
.fi
.\"
.TP
.B stop on \fIEVENT \fR[[\fIKEY=\fR]\fIVALUE\fR]... [\fBand\fR|\fBor\fR...]
The
.B stop on
stanza defines the set of events that will cause the job to be automatically
stopped.  It has the same syntax as
.B start on

.I VALUE
may additionally expand the value of any variable that came from the
job's start environment (either the event or the command that started it).

.nf
stop on stopping gdm or stopping kdm

stop on device-removed DEVPATH=$DEVPATH
.fi
.\"
.SS Job environment
Each job is run with the environment from the events or commands that
started it.  In addition, you may define defaults in the job which may
be overridden later and specify which environment variables are exported
into the events generated for the job.

The special
.B UPSTART_EVENTS
environment variable contains the list of events that started the job,
it will not be present if the job was started manually.

In addition, the
.B pre-stop
and
.B post-stop
scripts are run with the environment of the events or commands that
stopped the job.  THe
.B UPSTART_STOP_EVENTS
environment variable contains the list of events that stopped the job,
it will not be present if the job was stopped manually.

All jobs also contain the
.B UPSTART_JOB
and
.B UPSTART_INSTANCE
environment variables, containing the name of the job and instance.  These
are mostly used by the
.BR initctl (8)
utility to default to acting on the job the commands are called from.

.TP
.B env \fIKEY\fR[=\fIVALUE\fR]
Defines a default environment variable, the value of which may be overriden
by the event or command that starts the job.
If no value is given, then the value is taken from the
.BR init (8)
daemon's own environment.
.\"
.TP
.B export \fIKEY\fR
Exports the value of an environment variable into the
.BR starting (7),
.BR started (7),
.BR stopping (7)
and
.BR stopped (7)
events for this job.
.\"
.SS Services, tasks and respawning
Jobs are
.I services
by default.  This means that the act of starting the job is considered
to be finished when the job is running, and that even exiting with a
zero exit status means the service will be respawned.

.TP
.B task
This stanza may be used to specify that the job is a
.I task
instead.  This means that the act of starting the job is not considered
to be finished until the job itself has been run and stopped again, but
that existing with a zero exit status means the task has completed
successfully and will not be respawned.

.PP
The
.BR start (8)
command, and any
.BR starting (7)
or
.BR stopping (7)
events will block only until a service is running or until a task has
finished.

.TP
.B respawn
A service or task with this stanza will be automatically started if it
should stop abnormally.  All reasons for a service stopping, except
the
.BR stop (8)
command itself, are considered abnormal.  Tasks may exit with a zero
exit status to prevent being respawned.
.\"
.TP
.B respawn limit \fICOUNT INTERVAL
Respawning is subject to a limit, if the job is respawned more than
.I COUNT
times in
.I INTERVAL
seconds, it will be considered to be having deeper problems and will
be stopped.

This only applies to automatic respawns and not the
.BR restart (8)
command.
.\"
.TP
.B normal exit \fISTATUS\fR|\fISIGNAL\fR...
Additional exit statuses or even signals may be added, if the job
process terminates with any of these it will not be considered to have
failed and will not be respawned.

.nf
normal exit 0 1 TERM HUP
.fi
.\"
.SS Instances
By default, only one instance of any job is permitted to exist at one
time.  Attempting to start a job when it's already starting or running
results in an error.

Multiple instances may be permitted by defining the names of those
instances.  If an instance with the same name is not already starting
or running, a new instance will be started instead of returning an
error.

.TP
.B instance \fINAME
This stanza defines the names of instances, on its own its not particularly
useful since it would just define the name of the single permitted instance,
however
.I NAME
expands any variable defined in the job's environment.

These will often be variables that you need to pass to the process anyway,
so are an excellent way to limit the instances.

.nf
instance $CONFFILE
exec /sbin/httpd -c $CONFFILE
.fi

.nf
instance $TTY
exec /sbin/getty -8 38300 $TTY
.fi

These jobs appear in the
.BR initctl (8)
output with the instance name in parentheses, and have the
.B INSTANCE
environment variable set in their events.
.\"
.SS Documentation
Upstart provides several stanzas useful for documentation and external
tools.

.TP
.B description \fIDESCRIPTION
This stanza may contain a description of the job.

.nf
description "This does neat stuff"
.fi
.\"
.TP
.B author \fIAUTHOR
This stanza may contain the author of the job, often used as a contact
for bug reports.

.nf
author "Scott James Remnant <scott@netsplit.com>"
.fi
.\"
.TP
.B version \fIVERSION
This stanza may contain version information about the job, such as revision
control or package version number.  It is not used or interpreted by
.BR init (8)
in any way.

.nf
version "$Id$"
.fi
.\"
.TP
.B emits \fIEVENT\fR...
All processes on the system are free to emit their own events by using the
.BR initctl (8)
tool, or by communicating directly with the
.BR init (8)
daemon.

This stanza allows a job to document in its job configuration what events
it emits itself, and may be useful for graphing possible transitions.
.\"
.SS Process environment
Many common adjustments to the process environment, such as resource
limits, may be configured directly in the job rather than having to handle
them yourself.

.TP
.B console output\fR|\fBowner
By default the standard input, output and error file descriptors of jobs
are connected to
.I /dev/null

If this stanza is specified, they are connected to
.I /dev/console
instead.

.B console owner
is special, it not only connects the job to the system console but sets
the job to be the owner of the system console, which means it will receive
certain signals from the kernel when special key combinations such as
Control-C are pressed.
.\"
.TP
.B umask \fIUMASK
A common configuration is to set the file mode creation mask for the
process.
.I UMASK
should be an octal value for the mask, see
.BR umask (2)
for more details.
.\"
.TP
.B nice \fINICE
Another common configuration is to adjust the process's nice value,
see
.BR nice (1)
for more details.
.\"
.TP
.B oom \fIADJUSTMENT\fR|\fBnever
Normally the OOM killer regards all processes equally, this stanza
advises the kernel to treat this job differently.

.I ADJUSTMENT
may be an integer value from
.I -16
(very unlikely to be killed by the OOM killer) up to
.I 14
(very likely to be killed by the OOM killer).  It may also be the special
value
.B never
to have the job ignored by the OOM killer entirely.
.\"
.TP
.B chroot \fIDIR
Runs the job's processes in a
.BR chroot(8)
environment underneath
.I DIR

Note that
.I DIR
must have all the necessary system libraries for the process to be run,
often including
.I /bin/sh
.\"
.TP
.B chdir \fIDIR
Runs the job's processes with a working directory of
.I DIR
instead of the root of the filesystem.
.\"
.TP
.B limit \fILIMIT SOFT\fR|\fBunlimited \fIHARD\fR|\fBunlimited
Sets initial system resource limits for the job's processes.
.I LIMIT
may be one of
.IR core ,
.IR cpu ,
.IR data ,
.IR fsize ,
.IR memlock ,
.IR msgqueue ,
.IR nice ,
.IR nofile ,
.IR nproc ,
.IR rss ,
.IR rtprio ,
.I sigpending
or
.IR stack .

Limits are specified as both a
.I SOFT
value and a
.I HARD
value, both of which are integers.  The special value
.B unlimited
may be specified for either.
.\"
.SS Miscellaneous
.TP
.B kill timeout \fIINTERVAL
Specifies the interval between sending the job's main process the
.I SIGTERM
and
.I SIGKILL
signals when stopping the running job.
.\"
.TP
.B expect stop
Specifies that the job's main process will raise the
.I SIGSTOP
signal to indicate that it is ready.
.BR init (8)
will wait for this signal before running the job's post-start script,
or considering the job to be running.

.BR init (8)
will send the process the
.I SIGCONT
signal to allow it to continue.
.\"
.TP
.B expect daemon
Specifies that the job's main process is a daemon, and will fork twice
after being run.
.BR init (8)
will follow this daemonisation, and will wait for this to occur before
running the job's post-start script or considering the job to be running.

Without this stanza
.BR init (8)
is unable to supervise daemon processes and will believe them to have
stopped as soon as they daemonise on startup.
.\"
.TP
.B expect fork
Specifies that the job's main process will fork once after being run.
.BR init (8)
will follow this fork, and will wait for this to occur before
running the job's post-start script or considering the job to be running.

Without this stanza
.BR init (8)
is unable to supervise forking processes and will believe them to have
stopped as soon as they fork on startup.
.\"
.SH SEE ALSO
.BR init (8)
.BR sh (1)
